

# Repositório com exemplos de uso do swoole

## Set up

adicione a definições de variáveis de ambiente em seu `~/.zshrc` (ou `~/.profile`)

```sh
export UID=$(id -u)
export GID=$(id -g)
```

no terminal

```sh
source ~/.zshrc # ou ~/.profile
```


```sh
make build
make up
make php
```


## Exemplos

1. sleep (sem corrotina)

```sh
time php examples/sleep1.php
```

2. sleep (com corrotina)

```sh
time php examples/sleep2.php
```

3. sleep (com corrotina)

```sh
time php examples/sleep3.php
```
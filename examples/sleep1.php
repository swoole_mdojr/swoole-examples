<?php

// examples/sleep1.php

(function () {
    sleep(2);
    echo "1";
})();

(function () {
    sleep(1);
    echo "2";
})();
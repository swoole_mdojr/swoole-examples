<?php

// examples/sleep2.php

use function Swoole\Coroutine\run;
use function Swoole\Coroutine\go;

run(function() {
    go(function () {
        sleep(2);
        echo "1";
    });
    
    go(function () {
        sleep(1);
        echo "2";
    });
});
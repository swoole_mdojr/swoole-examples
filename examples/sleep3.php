<?php

// examples/sleep3.php

use function Swoole\Coroutine\run;
use function Swoole\Coroutine\go;

run(function () {
    go(function () {
        echo "1";
        sleep(2);
        echo "6";
    });

    echo "2";

    go(function () {
        echo "3";
        sleep(1);
        echo "5";
    });

    echo "4";
});
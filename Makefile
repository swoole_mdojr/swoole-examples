up:
	docker compose up || docker-compose up
build:
	docker compose build --no-cache main || docker-compose build --no-cache main
down:
	docker compose down || docker-compose down
php:
	docker exec -it swoole-presentation bash
